<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    $user = \App\User::firstOrNew(['name' => 'example6', 'email' => 'example6@ss.ss']);
    $user->inspectJson();
    //echo $user->settings()->profile_show."\n";
    //echo $user->settings()->sns_show;
    //$user->settings()->profile_show = 2;
    //$user->save();
    dd($user->settings()); die;
    //return view('welcome');
});


Route::get('/home', function() {
    return view('welcome');
});//->middleware('active');

// Login and Register
Route::get('login', [
            'as' => 'login',
            'uses' => 'Auth\LoginController@showLoginForm',
]);
Route::post('login', 'Auth\LoginController@login');

Route::get('logout', [
        'as'   => 'logout',
        'uses' => 'Auth\LoginController@logout',
]);

// Register
Route::get('register', [
           'as' => 'register',
           'uses' => 'Auth\RegisterController@showRegistrationForm',
           ]);
Route::post('register', 'Auth\RegisterController@register');

// user: using RegisterController, UserController
Route::group(['prefix' => 'user'], function () {
    // user activation
    Route::get('activation', [
               'as' => 'activation',
               'uses' => 'Auth\RegisterController@showActivationEmailForm']);
    Route::post('activation/send', 'Auth\RegisterController@sendActivationEmail');
    Route::get('activation/{token}', 'Auth\RegisterController@userActivation');

    Route::group(['middleware' => 'auth'], function () {
      // User Profile
      Route::get('profile/{id?}', 'Admin\UserController@showProfile');
      // Posts
      Route::resource('post', 'Admin\PostController');
    });
});


//password reset route
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('password/reset/{token?}', 'Auth\ResetPasswordController@showResetForm');


// Social Login
Route::get('socialAuth/{provider?}', 'Auth\LoginController@socialAuthenticate');
Route::get('socialAuth/callback/{provider?}', 'Auth\LoginController@socialLogin');

Route::group(['middleware' => 'auth'], function () {
    // Sites
    Route::resource('site', 'Admin\SiteController');

    // Social
    Route::group(['prefix' => 'social'], function () {
        Route::get('/', ['as' => 'social.index',
                        'uses' => 'Admin\SocialController@index']);

    });

    // Admin main controller
    Route::get('main', 'Admin\IndexController@index');
});
