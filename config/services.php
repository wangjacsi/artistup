<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '149044368881024',
        'client_secret' => '270788974ccce217bc457ca9c0a24ee3',
        'redirect' => 'http://localhost:8000/socialAuth/callback/facebook',
    ],

    'google' => [
        'client_id' => '194080132818-o9gu4t4ha0hhaaua1flpusdvjh26q9r5.apps.googleusercontent.com',
        'client_secret' => 'IACndwFdu_YXluiSIk-cFCtT',
        'redirect' => 'http://localhost:8000/socialAuth/callback/google',
    ],


];
