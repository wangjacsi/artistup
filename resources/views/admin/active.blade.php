@extends('admin.layouts.frame')

@section('styles')

@stop

@section('title')
{{ config('app.name').' | Activation' }}
@stop

@section('script_head')
<script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/pages/css/windows.chrome.fix.css') }}" />'
    }
</script>
@stop

@section('content')
<body class="fixed-header ">
  <div class="login-wrapper ">
    @include('admin.layouts.loginBG')
    <!-- START Login Right Container-->
    <div class="login-container bg-white">
      <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
        <img src="{{ URL::asset('assets/admin/assets/img/logo.png') }}" alt="logo" data-src="{{ URL::asset('assets/admin/assets/img/logo.png') }}" data-src-retina="{{ URL::asset('assets/admin/assets/img/logo_2x.png') }}" width="78" height="22">
        <p class="p-t-35">가입하신 이메일의 사용자 인증을 위하여 이메일을 발송합니다. 가입하신 이메일 주소를 넣어주세요.</p>

        <!-- START Login Form -->
        <form id="form-login" class="p-t-15" role="form" action="{{ url('user/activation/send') }}" method="POST">
          <!-- START Form Control-->
          {{ csrf_field() }}

          <div class="form-group form-group-default {{ $errors->has('email') ? "has-error" : "" }}">
            <label>이메일</label>
            <div class="controls">
              <input type="email" name="email" placeholder="등록하신 이메일 주소를 기입해 주세요" class="form-control" value="{{ old('email') }}" required>
            </div>
          </div>
          <!--{!! $errors->first('email', '<label id="email-error" class="error" for="email">:message</label>') !!}-->
          <!-- END Form Control-->

         @if (count($errors) > 0)
        <div class="alert alert-danger" role="alert">
          <button class="close" data-dismiss="alert"></button>
          @foreach ($errors->all() as $error)
              {{ $error }}<br>
          @endforeach
        </div>
        @endif

        @if (session('notice'))
            <div class="alert alert-success" role="alert">
                <button class="close" data-dismiss="alert"></button>
                {{ session('notice') }}
            </div>
        @endif

          <!-- END Form Control-->
          <button class="btn btn-primary btn-cons m-t-10 width-49" type="submit">보내기</button>
          <a href="{{ route('login') }}" class="btn btn-complete btn-cons m-t-10 width-49" type="">로그인</a>
        </form>

      </div>
    </div>
    <!-- END Login Right Container-->
  </div>

  @include('admin.layouts.overlay')

@stop


@section('scripts')
<!-- BEGIN VENDOR JS -->
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-validation/js/additional-methods.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-validation/js/localization/messages_ko.js') }}" type="text/javascript"></script>
<!-- END VENDOR JS -->
@stop

@section('scripts_last')
<script>
$(function()
{
  $('#form-login').validate()
})
</script>
@stop
