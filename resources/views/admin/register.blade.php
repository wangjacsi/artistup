@extends('admin.layouts.frame')

@section('styles')

@stop

@section('title')
{{ config('app.name').' | Register' }}
@stop

@section('script_head')
<script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/pages/css/windows.chrome.fix.css') }}" />'
    }
</script>
@stop

@section('content')
<body class="fixed-header ">
  <div class="login-wrapper ">
    @include('admin.layouts.loginBG')

    <!-- START Login Right Container-->
    <div class="login-container bg-white">
      <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
        <img src="{{ URL::asset('assets/admin/assets/img/logo.png') }}" alt="logo" data-src="{{ URL::asset('assets/admin/assets/img/logo.png') }}" data-src-retina="{{ URL::asset('assets/admin/assets/img/logo_2x.png') }}" width="78" height="22">
        <p class="p-t-35">반갑습니다. 서비스 가입을 환영합니다.</p>
        <!-- START Login Form -->
        <form id="form-register" class="p-t-15 m-b-30" role="form" action="{{ route('register') }}" method="POST">
            {{ csrf_field() }}
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>이메일</label>
                    <input type="email" name="email" placeholder="이메일 주소를 기입해주세요." class="form-control" required value="{{ old('email') }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>사용자 이름</label>
                    <input type="text" name="name" placeholder="이름을 설정해주세요." class="form-control" required value="{{ old('name') }}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>패스워드</label>
                    <input type="password" name="password" placeholder="최소 6자 이상" class="form-control" required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group form-group-default">
                    <label>패스워드 확인</label>
                    <input type="password" name="password_confirmation" placeholder="최소 6자 이상" class="form-control" required>
                  </div>
                </div>
              </div>
              <div class="row m-t-10">
                <div class="col-md-6">
                  <p><a href="#" class="text-info">서비스 이용약관</a>과 <a href="#" class="text-info"> 개인정보취급방침</a>에 동의합니다.</p>
                </div>
                <div class="col-md-6 text-right">
                  <a href="{{ url('/password/reset') }}" class="text-info small">비밀번호 재설정</a>
                </div>
              </div>

              @if (count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                <button class="close" data-dismiss="alert"></button>
                <strong>등록 실패</strong><br>
                @foreach ($errors->all() as $error)
                    {{ $error }}<br>
                @endforeach
              </div>
              @endif

              @if (session('notice'))
                  <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      {{ session('notice') }}
                  </div>
              @endif

              @if (session('warning'))
                  <div class="alert alert-danger" role="alert">
                    <button class="close" data-dismiss="alert"></button>
                    {{ session('warning') }}
                  </div>
              @endif

              <button class="btn btn-primary btn-cons m-t-10 width-49" type="submit">가입하기</button>
              <a href="{{ route('login') }}" class="btn btn-complete btn-cons m-t-10 width-49" type="">로그인</a>
            </form>

            <a class="btn btn-block btn-info m-t-30" type="" href="{{ url('socialAuth/facebook') }}">
                <span class="pull-left"><i class="fa fa-facebook"></i>
                              </span>
                <span class="bold">Facebook 가입</span>
            </a>
            <a class="btn btn-block btn-danger m-b-30" type="" href="{{ url('socialAuth/google') }}">
                <span class="pull-left"><i class="fa fa-google-plus"></i>
                              </span>
                <span class="bold">Google+ 가입</span>
            </a>
            <div class="row">
                <div class="col-md-12 m-t-10">
                  <a href="{{ route('activation') }}" class="text-info small underline">사용자 인증 이메일 보내기</a>
                </div>
            </div>
        <!--END Login Form-->
        <!--<div class="pull-bottom sm-pull-bottom">
          <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
            <div class="col-sm-3 col-md-2 no-padding">
              <img alt="" class="m-t-5" data-src="{{ URL::asset('assets/admin/assets/img/demo/pages_icon.png') }}" data-src-retina="{{ URL::asset('assets/admin/assets/img/demo/pages_icon_2x.png') }}" height="60" src="{{ URL::asset('assets/admin/assets/img/demo/pages_icon.png') }}" width="60">
            </div>
            <div class="col-sm-9 no-padding m-t-10">
              <p>
                <small>
                Create a pages account. If you have a facebook account, log into it for this process. Sign in with <a href="#" class="text-info">Facebook</a> or <a href="#" class="text-info">Google</a></small>
              </p>
            </div>
          </div>
        </div>-->
      </div>
    </div>
    <!-- END Login Right Container-->
  </div>

  @include('admin.layouts.overlay')

@stop


@section('scripts')
<!-- BEGIN VENDOR JS -->
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-validation/js/additional-methods.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-validation/js/localization/messages_ko.js') }}" type="text/javascript"></script>
<!-- END VENDOR JS -->
@stop

@section('scripts_last')
<script>
$(function()
{
  $('#form-register').validate()
})
</script>
@stop
