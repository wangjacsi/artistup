@extends('admin.layouts.frame')

@section('styles')
<link href="{{ URL::asset('assets/admin/assets/plugins/mapplic/css/mapplic.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('title')
{{ config('app.name').' | Main' }}
@stop

@section('script_head')
<script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/pages/css/windows.chrome.fix.css') }}" />'
    }
</script>
@stop

@section('content')
<body class="fixed-header no-header">
    @include('admin.layouts.sidebar')

    <!-- START PAGE-CONTAINER -->
    <div class="page-container ">
      @include('admin.layouts.header')
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid padding-25 sm-padding-10">
              <div class="row">
                <div class="col-md-8 ">
                  <div class="map-container full-width full-height relative">
                    <div class="map-controls">
                      <div class="pull-left">
                        <div class="btn-group btn-group-vertical" data-toggle="buttons-radio">
                          <button class="btn btn-xs"><i class="fa fa-plus"></i>
                          </button>
                          <button class="btn btn-xs"><i class="fa fa-minus"></i>
                          </button>
                        </div>
                        <br>
                        <a href="#" class="btn btn-xs m-t-10 clear-map">
                          <i class="fa fa-arrows"></i>
                        </a>
                      </div>
                      <div class="pull-left m-l-15">
                        <form role="form">
                          <div class="form-group form-group-default form-group-default-select2">
                            <label>Country</label>
                            <select id="country-list" data-placeholder="Search by locationg, tag, ID" data-init-plugin="select2">
                            </select>
                          </div>
                        </form>
                      </div>
                    </div>
                    <div id="mapplic"></div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <!-- END PAGE CONTENT -->

        @include('admin.layouts.footer')

      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->

    @include('admin.layouts.quickview')

    @include('admin.layouts.overlay')
@stop


@section('scripts')
<!-- BEGIN VENDOR JS -->
<script src="{{ URL::asset('assets/admin/assets/plugins/mapplic/js/hammer.js') }}"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/mapplic/js/jquery.mousewheel.js') }}"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/mapplic/js/mapplic.js') }}"></script>


<!-- END VENDOR JS -->
@stop

@section('scripts_last')
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ URL::asset('assets/admin/assets/js/vector_map.js') }}" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ URL::asset('assets/admin/assets/js/scripts.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
@stop
