@extends('admin.layouts.frame')

@section('styles')

@stop

@section('title')
{{ config('app.name').' | Login' }}
@stop

@section('script_head')
<script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/admin/pages/css/windows.chrome.fix.css') }}" />'
    }
</script>
@stop

@section('content')
<body class="fixed-header ">
  <div class="login-wrapper ">
    @include('admin.layouts.loginBG')
    <!-- START Login Right Container-->
    <div class="login-container bg-white">
      <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
        <img src="{{ URL::asset('assets/admin/assets/img/logo.png') }}" alt="logo" data-src="{{ URL::asset('assets/admin/assets/img/logo.png') }}" data-src-retina="{{ URL::asset('assets/admin/assets/img/logo_2x.png') }}" width="78" height="22">
        <p class="p-t-35">반갑습니다. 로그인 하세요.</p>

        <!-- START Login Form -->
        <form id="form-login" class="p-t-15" role="form" action="{{ route('login') }}" method="POST">
          <!-- START Form Control-->
          {{ csrf_field() }}

          <div class="form-group form-group-default {{ $errors->has('email') ? "has-error" : "" }}">
            <label>이메일</label>
            <div class="controls">
              <input type="email" name="email" placeholder="등록하신 이메일 주소를 기입해 주세요" class="form-control" value="{{ old('email') }}" required>
            </div>
          </div>
          <!--{!! $errors->first('email', '<label id="email-error" class="error" for="email">:message</label>') !!}-->
          <!-- END Form Control-->
          <!-- START Form Control-->
          <div class="form-group form-group-default {{ $errors->has('password') ? "has-error" : "" }}">
            <label>패스워드</label>
            <div class="controls">
              <input type="password" class="form-control" name="password" placeholder="사용자 비밀번호" required>
            </div>
          </div>
          <!--{!! $errors->first('password', '<label id="password-error" class="error" for="password">:message</label>') !!}-->
          <!-- START Form Control-->
          <div class="row">
            <div class="col-md-5 col-xs-5 ">
              <div class="checkbox ">
                <input type="checkbox" value="1" id="checkbox1" name="remember" value="{{ old('remember') }}">
                <label for="checkbox1">로그인 정보 저장</label>
              </div>
            </div>
            <div class="col-md-6 col-xs-6 text-right m-t-10">
              <a href="{{ url('/password/reset') }}" class="text-info small">비밀번호 재설정</a>
            </div>
          </div>

          @if (count($errors) > 0)
        <div class="alert alert-danger" role="alert">
          <button class="close" data-dismiss="alert"></button>
          <strong>로그인 실패</strong><br>
          이메일 또는 패스워드를 다시 한번 확인하시기 바랍니다.
        </div>
        @endif

        @if (session('notice'))
            <div class="alert alert-success" role="alert">
                <button class="close" data-dismiss="alert"></button>
                {{ session('notice') }}
            </div>
        @endif

        @if (session('warning'))
            <div class="alert alert-danger" role="alert">
              <button class="close" data-dismiss="alert"></button>
              {{ session('warning') }}
            </div>
        @endif

          <!-- END Form Control-->
          <button class="btn btn-primary btn-cons m-t-10 width-49" type="submit">로그인</button>
          <a href="{{ route('register') }}" class="btn btn-complete btn-cons m-t-10 width-49" type="">가입하기</a>
        </form>

        <a class="btn btn-block btn-info m-t-30" type="" href="{{ url('socialAuth/facebook') }}">
            <span class="pull-left"><i class="fa fa-facebook"></i>
                          </span>
            <span class="bold">Facebook 로그인</span>
        </a>
        <a class="btn btn-block btn-danger m-b-30" type="" href="{{ url('socialAuth/google') }}">
            <span class="pull-left"><i class="fa fa-google-plus"></i>
                          </span>
            <span class="bold">Google+ 로그인</span>
        </a>
        <div class="row">
            <div class="col-md-12 m-t-10">
              <a href="{{ route('activation') }}" class="text-info small underline">사용자 인증 이메일 보내기</a>
            </div>
        </div>
        <!--END Login Form-->
        <!--<div class="pull-bottom sm-pull-bottom">
          <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
            <div class="col-sm-3 col-md-2 no-padding">
              <img alt="" class="m-t-5" data-src="{{ URL::asset('assets/admin/assets/img/demo/pages_icon.png') }}" data-src-retina="{{ URL::asset('assets/admin/assets/img/demo/pages_icon_2x.png') }}" height="60" src="{{ URL::asset('assets/admin/assets/img/demo/pages_icon.png') }}" width="60">
            </div>
            <div class="col-sm-9 no-padding m-t-10">
              <p>
                <small>
                Create a pages account. If you have a facebook account, log into it for this process. Sign in with <a href="#" class="text-info">Facebook</a> or <a href="#" class="text-info">Google</a></small>
              </p>
            </div>
          </div>
        </div>-->
      </div>
    </div>
    <!-- END Login Right Container-->
  </div>



@stop


@section('scripts')
<!-- BEGIN VENDOR JS -->
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-validation/js/jquery.validate.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-validation/js/additional-methods.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-validation/js/localization/messages_ko.js') }}" type="text/javascript"></script>
<!-- END VENDOR JS -->
@stop

@section('scripts_last')
<script>
$(function()
{
  $('#form-login').validate()
})
</script>
@stop
