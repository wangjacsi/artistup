<script src="{{ URL::asset('assets/admin/assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/bootstrapv3/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-bez/jquery.bez.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-actual/jquery.actual.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/assets/plugins/classie/classie.js') }}"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/admin/assets/plugins/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>

@yield('scripts')

<script src="{{ URL::asset('assets/admin/pages/js/pages.min.js') }}"></script>

@yield('scripts_last')