<!-- START Login Background Pic Wrapper-->
<div class="bg-pic">
  <!-- START Background Pic-->
  <img src="{{ URL::asset('assets/admin/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg') }}" data-src="{{ URL::asset('assets/admin/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg') }}" data-src-retina="{{ URL::asset('assets/admin/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg') }}" alt="" class="lazy">
  <!-- END Background Pic-->
  <!-- START Background Caption-->
  <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
    <h2 class="semi-bold text-white">우리 삶의 일상은 그 자체가 예술이다<br>- R Kelly</h2>
    <p class="small">
      이미지는 작가 Quatrine님의 작품 '오늘의 하루' 입니다.<br>All work copyright of respective owner, otherwise © 2016 {{ config('app.name') }}.
    </p>
  </div>
  <!-- END Background Caption-->
</div>
<!-- END Login Background Pic Wrapper-->