<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="{{ URL::asset('assets/admin/pages/ico/60.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('assets/admin/pages/ico/76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ URL::asset('assets/admin/pages/ico/120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('assets/admin/pages/ico/152.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ URL::asset('favicon.ico') }}" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link href="{{ URL::asset('assets/admin/assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/admin/assets/plugins/bootstrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/admin/assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/admin/assets/plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ URL::asset('assets/admin/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ URL::asset('assets/admin/assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ URL::asset('assets/admin/pages/css/pages-icons.css') }}" rel="stylesheet" type="text/css">

    @yield('styles')
    <link class="main-stylesheet" href="{{ URL::asset('assets/admin/pages/css/themes/simple.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/admin/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="{{ URL::asset('assets/admin/pages/css/ie9.css') }}" rel="stylesheet" type="text/css" />
    <![endif]-->

    @yield('script_head')

</head>