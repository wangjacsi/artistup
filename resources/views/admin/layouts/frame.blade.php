<!DOCTYPE html>
<html>

@include('admin.layouts.head')

@yield('content')

@include('admin.layouts.scripts')

</body>
</html>