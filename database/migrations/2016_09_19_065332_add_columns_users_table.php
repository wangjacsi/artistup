<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('avatar')->nullable();
            $table->string('nickname')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->string('ip_address')->nullable();
            $table->enum('active', ['active', 'inactive', 'dormant', 'delected'])->default('inactive');
            $table->enum('level', ['lv1', 'lv2','lv3','lv4','lv5','lv6','lv7','lv8','lv9'])->default('lv1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['avatar', 'nickname', 'last_login', 'ip_address', 'active', 'level']);
        });
    }
}
