<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('phone', 20)->nullable();
            $table->string('country', 50)->nullable();
            $table->string('country_code', 2)->nullable();
            $table->string('affiliation', 100)->nullable();
            $table->string('position',20)->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->enum('gender', ['female', 'male', 'ignore'])->default('ignore');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
