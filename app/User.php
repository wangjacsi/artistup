<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
use Bluora\LaravelModelJson\JsonColumnTrait;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use JsonColumnTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'avatar', 'nickname', 'last_login',
        'ip_address', 'level', 'is_activated',
        //'settings',
    ];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Settings with Json
    protected $json_columns = [
        'settings'
    ];

    protected $json_defaults = [
        'settings' => ['profile_show' => 1, 'sns_show' => 1, 'relation_show' => 1,
            'admin_theme' => ['title' => '', 'sub_title' => '']
        ]
    ];

    protected $json_options = [
        'settings' => ['no_saving_default_values' => true]
    ];

    /*********** Notifications ************************************************/
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /*********** Database Relation ********************************************/
    public function socialProfiles()
    {
        return $this->hasMany(SocialProfile::class);
    }

    public function profile(){
        return $this->hasOne(Profiles::class);
    }
}
