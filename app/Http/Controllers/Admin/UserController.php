<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    public function showProfile($id = null){
        // get user's profile
        //
        if(is_null($id)){
            $user = Auth::user();
        } else {
            $user = User::find($id);
        }
        $profile = $user->profile()->get();

        return view('admin.user.profile')->with(['user' => $user, 'profile' => $profile]);
    }
}
