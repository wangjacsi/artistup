<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Notifications\UserEmailActivationNotification;
use DB;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');//, ['except' => 'showActivationEmailForm']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    // Show Register Form Page
    public function showRegistrationForm()
    {
        return view('admin.register');
    }

    // Show Activtion Email Send Form
    public function showActivationEmailForm(){
        return view('admin.active');
    }

    // Custom register function here
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $this->create($request->all());

        // send user activation email
        $this->sendActivationEmail($request);
        return redirect()->route('login')
        ->with('notice', '가입을 환영합니다. 사용자 인증을 위한 이메일을 전송하였습니다. 이메일을 확인하시고 사용자 인증을 완료해 주세요.');
    }

    /**
     * Check for user Activation Code
     *
     * @param  array  $data
     * @return User
     */
    public function userActivation(Request $request, $token)
    {
        $check = DB::table('user_activations')->where('token',$token)->first();
        $email = $request->email;
        if(!is_null($check) && $check->email == $email){
            $user = User::where('email', $email)->firstOrFail();

            if($user->is_activated == 1){
                return redirect()->route('login')
                    ->with('notice',"이미 인증이 완료된 사용자입니다. 로그인해 주세요.");
            }
            $user->update(['is_activated' => 1]);
            DB::table('user_activations')->where('token',$token)->delete();

            return redirect()->route('login')
                ->with('notice',"이메일 인증이 완료되었습니다. 로그인해 주세요.");
        }
        return redirect()->to('login')
                ->with('warning',"이메일 인증에 실패하였습니다. 인증을 위한 메일을 다시 전송해보세요.");
    }

    // send to user's email for activation
    public function sendActivationEmail(Request $request){
        // send user activation email
        $user = User::where('email', $request['email'])->first();
        if($user){
            if($user->is_activated==1){
                return redirect()->route('login')
                    ->with('notice',"이메일 인증이 완료되었습니다. 로그인해 주세요.");
            }
            $token = $this->getToken();//str_random(30);
            $check = DB::table('user_activations')->where('email',$request['email'])->first();

            $current_time = Carbon::now();
            if(!is_null($check)){ // update
                DB::table('user_activations')->where('email', $request['email'])
                ->update(['token' => $token, 'updated_at' => $current_time]);
                $message = '사용자 인증을 위한 이메일을 전송하였습니다. 이메일을 확인하시고 사용자 인증을 완료해 주세요.';
            } else { //add new
                DB::table('user_activations')->insert([
                    'user_id'=>$user->id,
                    'email'=>$request['email'],'token'=>$token,
                    'created_at' => $current_time,
                    'updated_at' => $current_time]);
                $message = '가입을 환영합니다. 사용자 인증을 위한 이메일을 전송하였습니다. 이메일을 확인하시고 사용자 인증을 완료해 주세요.';
            }
            $user->notify(new UserEmailActivationNotification($token, $request['email']));
            return redirect()->route('login')->with('notice', $message);
        } else {
            return redirect()->back()
                ->with('warning', '사용자 등록이 되어 있지 않는 이메일입니다. 등록후에 인증처리를 받으세요.');
        }
    }

    protected function getToken()
    {
        return hash_hmac('sha256', str_random(40), config('app.key'));
    }
}
