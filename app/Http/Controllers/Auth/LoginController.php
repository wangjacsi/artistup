<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Exception;
use Socialite;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\SocialProfile;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/main';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }


    public function socialAuthenticate($provider)
    {
        if(!config("services.$provider")) abort('404');
        return Socialite::driver($provider)->redirect();
    }

    public function socialLogin(Request $request, $provider)
    {
        try {
            $socialUserInfo = Socialite::driver($provider)->user();
            //dd($socialUserInfo->id);

            // check this user in social table with provider('facebook') and
            // provider_user_id is facebook user id
            $oauth_identity = SocialProfile::firstOrNew(['provider' => $provider, 'provider_user_id' => $socialUserInfo->id]);
            $current_time = Carbon::now();

            // check users table by email : this user already registered
            if ($oauth_identity->user_id) { // 이미 소셜 로그인 정보 있을시
                $user = User::findOrFail($oauth_identity->user_id);
                $user->update([
                      'name'=>$socialUserInfo->name,
                      'avatar'=>$socialUserInfo->avatar,
                      'nickname' => $socialUserInfo->nickname,
                      'last_login' => $current_time,
                      'ip_address' => $request->ip(),
                      'is_activated' => 1
                      ]);
            } else { // 소셜 로그인 정보 없을 시
                //$user = User::where('email', $socialUserInfo->email)->first();
                $user = User::firstOrNew(['email' => $socialUserInfo->email]);
                $user->name = $socialUserInfo->name;
                $user->avatar = $socialUserInfo->avatar;
                $user->nickname = $socialUserInfo->nickname;
                $user->last_login = $current_time;
                $user->ip_address = $request->ip();
                $user->is_activated = 1;
                $user->save();
            }
            $oauth_identity->user_id = $user->id;
            $oauth_identity->access_token = $socialUserInfo->token;
            $oauth_identity->save();
            auth()->login($user);
            return redirect($this->redirectTo);
        } catch (Exception $e) {
            return redirect()->route('login')->with('warning', "$provider 로그인에 실패하였습니다.");

        }
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            if($this->guard()->user()->is_activated == '0'){
                $this->guard()->logout();
                return redirect()->back()->with('warning',"사용자 등록을 위한 이메일 인증을 받으세요.");
            }
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if (! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }
        return $this->sendFailedLoginResponse($request);
    }
}
